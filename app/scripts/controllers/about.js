'use strict';

/**
 * @ngdoc function
 * @name smykFrontendApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the smykFrontendApp
 */
angular.module('smykFrontendApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

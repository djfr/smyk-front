'use strict';

/**
 * @ngdoc function
 * @name smykFrontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the smykFrontendApp
 */
angular.module('smykFrontendApp')
  .controller('MainCtrl', function ($scope, $http) {
    $scope.submit = function () {
      if ($scope.inputs && $scope.inputs.leafLength && $scope.inputs.leafWidth && $scope.inputs.petalLength && $scope.inputs.petalWidth) {
        var data = {
          "leaf-length": $scope.inputs.leafLength,
          "leaf-width": $scope.inputs.leafWidth,
          "petal-length": $scope.inputs.petalLength,
          "petal-width": $scope.inputs.petalWidth
        };
        $http({
          method: 'POST',
          url: 'http://localhost:3000/flower',
          data: $scope.inputs
        }).then(function successCallback(response) {
          var flower = {name: '', value: 0};
          for (var par in response.data) {
            if (response.data[par] > flower.value) {
              flower.name = par;
              flower.value = response.data[par];
            }
          }
          $scope.answer = response.data;
          //$scope.answer = 'Looks like Your flower is "' + flower.name + '". I am sure about it on ' + Math.ceil(flower.value*100) + ' percent.'


        })
      } else {
        $scope.answer = "Please, fill all fields before search!"
      }
    };



    var nodes =  [
      {
        "leaf-length": 6.3,
        "leaf-width": 2.5,
        "petal-length": 5,
        "petal-width": 1.9,
        "class": "Iris-virginica"
      },
      {
        "leaf-length": 6.5,
        "leaf-width": 3,
        "petal-length": 5.2,
        "petal-width": 2,
        "class": "Iris-virginica"
      },
      {
        "leaf-length": 6.2,
        "leaf-width": 3.4,
        "petal-length": 5.4,
        "petal-width": 2.3,
        "class": "Iris-virginica"
      },
      {
        "leaf-length": 5.9,
        "leaf-width": 3,
        "petal-length": 5.1,
        "petal-width": 1.8,
        "class": "Iris-virginica"
      }
    ];

    d3.xhr('http://localhost:3000/structure', function (data) {
      nodes = JSON.parse(data.response);
      var classNames = [];
      var classConnection = {};
      var nodesResort = {};
      var g = [];

      //создаем массив дляпересортировки параметров по возрастанию, да
      nodes.forEach(function(item, i) {

        for (var par in item) {
          if (item.hasOwnProperty(par) && ['central', 'class'].indexOf(par) === -1) {
            if(Array.isArray(nodesResort[par])){
              nodesResort[par].push(
                {
                  text: item[par],
                  index: i
                }
              )
            }else{
              nodesResort[par] = [];
              nodesResort[par].push(
                {
                  text: item[par],
                  index: i
                }
              )
            }
          }
        }
        if(!classConnection[item.class]) {
          classConnection[item.class] = [];
        }
        classConnection[item.class].push(i);
        if (classNames.indexOf(item.class) === -1) {
          classNames.push(item.class)
        }
      });
      //пересортировуем каждый параметр так, чтобы данные в массиве были упорядочены по возрастанию, но сохраняем индекс исходного элемента, он нам еще понадобится

      var nodesMap = {};
      var nodesCollapserPar = {};
      var nodesHighLight = {};

      for (var par in nodesResort) {
        if (nodesResort.hasOwnProperty(par) && ['central', 'class'].indexOf(par) === -1) {
          nodesResort[par].sort(function(a, b) {
            return a.text - b.text;
          });
          var currentValues = nodesResort[par].map(function(item, i){
            return item.text
          });
          nodesMap[par]={};
          nodesCollapserPar[par]=[];
          nodesHighLight[par]=[];
          _.forEachRight(currentValues, function(item, i, arr){
            if (arr.indexOf(item) > -1) {
              if(!nodesHighLight[par][arr.indexOf(item)]) {
                nodesHighLight[par][arr.indexOf(item)] = [];
              }
              nodesHighLight[par][arr.indexOf(item)].push(i);
              nodesMap[par][i] = arr.indexOf(item);
            }
          });
          _.forOwn(nodesMap[par], function(value, key) {
            if (nodesCollapserPar[par].indexOf(value) === -1) {
              nodesCollapserPar[par].push(value)
            }
          });
          var tmp = {};
          nodesCollapserPar[par].forEach(function(item, i){
            tmp[item] = i;
          });
          nodesCollapserPar[par] = tmp;
        }

      }



      var height = 900,
        base = 30,//размер квадратиков и кружочков
      //width = Math.max((_.keys(nodesCollapserPar['leaf-width']).length + _.keys(nodesCollapserPar['leaf-length']).length ),(_.keys(nodesCollapserPar['petal-width']).length + _.keys(nodesCollapserPar['petal-length']).length + classNames.length)) * base*3;//считаем ширину готового графа
      width = (nodes.length*2 + classNames.length) * base*1.5;//считаем ширину готового графа


      var color = d3.scale.category20();



      var svg = d3.select("#mainContainer").append("svg")
        .attr("width", width)
        .attr("height", height);
    //здесь же мы указываем координаты для того, чтобы нарисовать квадратик с значением того или иного параметра, и сохраняем координаты для того, чтоб связать "нейроны" с индексом исходных данных


    nodesResort["leaf-length"].forEach(function(item, i) {
      item['x'] = (width / (_.keys(nodesCollapserPar['leaf-length']).length * 2)) * i;
      //item['xAssoc'] = (width / (nodes.length * 2)) * item.index;
      item['y'] = base;
    });
    nodesResort["leaf-width"].forEach(function(item, i) {
      item['x'] = (width / (_.keys(nodesCollapserPar['leaf-width']).length * 2)) * i + width / 2;
      //item['xAssoc'] = (width / (nodes.length * 2)) * item.index + width / 2;
      item['y'] = base;
    });
    nodesResort["petal-length"].forEach(function(item, i) {
      item['befooz'] = i;
      item['x'] = ((width - (classNames.length * base * 3)) / (_.keys(nodesCollapserPar['petal-length']).length * 2)) * i;
      //item['xAssoc'] = ((width - (classNames.length * base * 3)) / (nodes.length * 2)) * item.index;
      item['y'] = height - base * 2;
    });
    nodesResort["petal-width"].forEach(function(item, i) {
      item['x'] = ((width-(classNames.length*base*3))/(_.keys(nodesCollapserPar['petal-width']).length*2))*i+width/2 + (classNames.length*base*3)/2;
      //item['xAssoc'] = ((width-(classNames.length*base*3))/(nodes.length*2))*item.index+width/2 + (classNames.length*base*3)/2;
      item['y'] = height - base*2;
    });

//создаем данные для шариков
    nodes.forEach(function(item, i) {
      g.push(svg.append("g")
        .attr("class", "node g"+i));
      item['central'] = {
        text: 'g'+i,
        x: (width/(nodes.length))*i,
        y: height/2
      }
    });
//рисуем имена кааассов
    classNames.forEach(function(item, i){
      var node = g[i].append("g")
        .attr("class", "node classclass#"+item)
        .style("transform", 'translate(' + ((width/2 - (classNames.length*base*3)/2) + base*3*i) + 'px,' + (height - base*2) + 'px)');
      var rect = node.append("rect")
        .attr("class", "node")
        .attr("width", base*2)
        .attr("height", base)
        .style("fill", function (d) {
          return color(7);
        });
      node.append("text")
        .attr("dx", ".65em")
        .attr("dy", "1.35em")
        .text(item)
    });
//оисуем прямоуголтнички
    for (var param in nodesResort) {
      if (nodesResort.hasOwnProperty(param) && ['central', 'class'].indexOf(param) === -1) {
        nodesResort[param].forEach(function(item, i, thisArray) {
          g[item.index].append("line")
            .attr("class", "link")
            .attr("x1", thisArray[nodesCollapserPar[param][nodesMap[param][i]]].x+base)
            //.attr("x1", item.x+base)
            .attr("y1", item.y)
            .attr("x2", nodes[item.index]['central'].x+base*2)
            .attr("y2", nodes[item.index]['central'].y);
          var node = svg.append("g")
            .attr("class", "node param#"+param+'~'+i)
            .style("transform", 'translate(' + thisArray[nodesCollapserPar[param][nodesMap[param][i]]].x + 'px,' + item.y + 'px)');
          var rect = node.append("rect")
            .attr("class", "node")
            .attr("width", base*2)
            .attr("height", base)
            .style("fill", function (d) {
              return color(Object.keys(nodesResort).indexOf(param));
            });
          node.append("text")
            .attr("dx", ".65em")
            .attr("dy", "1.35em")
            .text(item.text)
        })
      }
    }
    nodes.forEach(function(item, i){
      g[i].append("line")
        .attr("class", "link")
        .attr("x1", ((width/2 - (classNames.length*base*3)/2) + base*3*(classNames.indexOf(item.class)))+base)
        .attr("y1", (height - base*2))
        .attr("x2", (item['central'].x+base*2))
        .attr("y2", item['central'].y);
    });
//рисуем шарики
    nodes.forEach(function(item, i) {
      var par = 'central';
      var node = g[i].append("g")
        .attr("class", "node")
        .style("transform", 'translate(' + (item[par].x+base*2) + 'px,' + item[par].y + 'px)');

      var circle = node.append("circle")
        .attr("class", "node")
        .attr("r", base)
        .style("fill", function (d) {
          return color(9);
        });
      node.append("text")
        .attr("dx", "-.35em")
        .attr("dy", ".35em")
        .text('g'+i)

    });

    //создаём надписи к параметрам
    var l1 = svg.append('g').attr("class", "label")
      .style("transform", 'translate(' + (0) + 'px,' + 0 + 'px)')
    l1.append('rect')
      .attr("width", width/2)
      .attr("height", base)
      .style("fill", function (d) {
        return color(0);
      });
    l1.append("text")
      .attr("dx", "0.65em")
      .attr("dy", "1.35em")
      .text(Object.keys(nodes[0])[0]);

    var l2 = svg.append('g').attr("class", "label")
      .style("transform", 'translate(' + (width/2) + 'px,' + 0 + 'px)')
    l2.append('rect')
      .attr("width", width/2)
      .attr("height", base)
      .style("fill", function (d) {
        return color(1);
      });
    l2.append("text")
      .attr("dx", "0.65em")
      .attr("dy", "1.35em")
      .text(Object.keys(nodes[0])[1]);

    var l3 = svg.append('g').attr("class", "label")
      .style("transform", 'translate(' + (0) + 'px,' + (height-base) + 'px)')
    l3.append('rect')
      .attr("width", (width-(classNames.length*base*3))/2)
      .attr("height", base)
      .style("fill", function (d) {
        return color(2);
      });
    l3.append("text")
      .attr("dx", "0.65em")
      .attr("dy", "1.35em")
      .text(Object.keys(nodes[0])[2]);
    var l5 = svg.append('g').attr("class", "label")
      .style("transform", 'translate(' + (width-(classNames.length*base*3))/2 + 'px,' + (height-base) + 'px)')
      l5.append('rect')
      .attr("width", (classNames.length*base*3))
      .attr("height", base)
      .style("fill", function (d) {
        return color(7);
      });
    l5.append("text")
      .attr("dx", "0.65em")
      .attr("dy", "1.35em")
      .text(Object.keys(nodes[0])[4]);
    var l4 = svg.append('g').attr("class", "label")
      .style("transform", 'translate(' + (width+(classNames.length*base*3))/2 + 'px,' + (height-base) + 'px)')
    l4.append('rect')
      .attr("width", (width-(classNames.length*base*3))/2)
      .attr("height", base)
      .style("fill", function (d) {
        return color(3);
      });
    l4.append("text")
      .attr("dx", "0.65em")
      .attr("dy", "1.35em")
      .text(Object.keys(nodes[0])[3]);
      //classConnection[item.class]


      $('[class*="classclass#"]').click( function(event) {
        var elemClassName = $(this).attr('class');
        var className = elemClassName.slice(elemClassName.indexOf('#')+1);
        if(classConnection[className]){
          classConnection[className].forEach(function(item){
            $('.g'+item).toggleClass('active');
          });
        }
      });
      var selectedParams = [];

      $('[class*="param#"]').click( function(event) {
        var elemClassName = '';

        if ( $(this).attr('class').indexOf('active-twice')>-1) {
          $(this).removeClass('active-twice');
          elemClassName = $(this).attr('class');
        } else {
          elemClassName = $(this).attr('class');
          $(this).addClass('active-twice');
        }
        var index = elemClassName.slice(elemClassName.indexOf('~')+1);
        var className = elemClassName.slice(elemClassName.indexOf('#')+1, elemClassName.indexOf('~'));

        if(nodesHighLight[className][nodesMap[className][index]] && selectedParams.indexOf(elemClassName)===-1){
          selectedParams.push(elemClassName);
          nodesHighLight[className][nodesMap[className][index]].forEach(function(item){
            if ($('.g'+nodesResort[className][item].index).attr('class').indexOf('active-twice')>-1) {
              $('.g'+nodesResort[className][item].index).addClass('active-iii');

            } else if($('.g'+nodesResort[className][item].index).attr('class').indexOf('active')>-1){
              $('.g'+nodesResort[className][item].index).addClass('active-twice');
            }else {
              $('.g'+nodesResort[className][item].index).addClass('active');
            }
          });
        }else{
          selectedParams.splice(selectedParams.indexOf(elemClassName), 1);
          nodesHighLight[className][nodesMap[className][index]].forEach(function(item){
            if ($('.g'+nodesResort[className][item].index).attr('class').indexOf('active-iii')>-1) {
              $('.g'+nodesResort[className][item].index).removeClass('active-iii');

            } else if ($('.g'+nodesResort[className][item].index).attr('class').indexOf('active-twice')>-1) {
              $('.g'+nodesResort[className][item].index).removeClass('active-twice');

            } else {
              $('.g'+nodesResort[className][item].index).removeClass('active');
            }
          });
        }
      });
      //console.log('ss', $(svg) );

    });


  });
